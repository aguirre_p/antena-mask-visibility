positio#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 11:32:58 2022

@author: pablo
"""

import rasterio
import pandas as pd
import numpy as np
  
    
def hor_coord_from_raster (rasterpath): 
    img = rasterio.open(rasterpath)
       
    #seleccionar la parte del array que tiene los valores
    array=img.read()
    array = np.array(array)
    
    #sustituir valor nan por -9999
    #array[np.isnan(array)]=-9999
    #filtrar los valores que no tengan valor -9999
    position = np.where(array > -1)
    
    #asignar coordenadas a cada punto
    
    filX = position[1]
    colY = position[2]
    
    rasarray = array[0]
    valZ = []
    for i in range(len(filX)):
        #tomar el valor de Z correspondiente a los valores que no son  -99999
        val = rasarray[filX[i]][colY[i]]
        valZ.append(val)
        
        
    ######geometry###########
    #get bounds
    bounds = img.bounds
    
    left = bounds[0]
    top = bounds[3]
       
    # pixel size
    widthunits = bounds[2]-bounds[0]
    heightunits = bounds[3]-bounds[1]
    
    pixwith = widthunits/img.width
    pixheight = heightunits/img.height
    
    #give pix coordinate geographic coordinates
    
    xgeographic = left + (colY*pixheight)+(pixwith/2)
    ygeographic = top - (filX*pixwith)-(pixheight/2)
    
    # montar el dataframe column por column
       
    
    dfX = pd.DataFrame(xgeographic,columns=['X'])
    
    dfY = pd.DataFrame(ygeographic,columns=['Y'])
    dfZ1 = pd.DataFrame(valZ,columns=['Z1'])
    #concatenar en el dataframe final
    hor_coord = pd.concat([dfX,dfY,dfZ1 ], axis=1)
    
    return(hor_coord)

rasterpath = '/media/pablo/LocalData/visibilityAnalysis/mde/visualHight.tif' 

test = hor_coord_from_raster(rasterpath)


   