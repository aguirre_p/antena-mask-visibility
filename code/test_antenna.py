#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 09:25:46 2022

@author: pablo
"""

import csv
import numpy as np
import pandas as pd
from numpy import genfromtxt
import matplotlib.pyplot as plt

def azimuthandaltitud (hor_coord,ant_coord):
    Ax = ant_coord['AX'][:1]
    Ay = ant_coord['AY'][:1]

    Hx = hor_coord[['X']]
    Hy = hor_coord[['Y']]
    
    Az = ant_coord['AZ'][:1]
    Hz = hor_coord[['Z1']]
    valAz = Az.values
    
    valHx = Hx.values
    valHy = Hy.values
    
    incX = int(Ax) - valHx
    incY = int(Ay) - valHy
    
    #calculate distance between antenna and horizont
    distace = Hx.apply(lambda x: np.sqrt((x+Ax[0])**2+(Hy['Y']+Ay[0])**2))
    distrn = distace.rename(columns={'X':'distance'})
    
    #calculate latitud in degreen of the horizont
    altitud = Hz.apply(lambda x: (np.degrees(np.arctan2(distrn['distance'],valAz[0]))))
    alta0 = altitud.fillna(0)
    
    #calculate the azimuth in degrees of the point of the horizont vs the antenna pint of view
    az = np.degrees(np.arctan2(incY,incX))
    azpd = pd.DataFrame(az)
    azpdrn = azpd.rename(columns={0:'az'})  
    pd360 = azpdrn['az'].apply(lambda x: x+360 if x<0 else x)
    
    
    
     
    
    #merge all thew values in one dataframe 
    
    azalt = pd.concat([pd360,alta0,distrn],axis=1)
    

    return(azalt)

#getting coords of the horizont
horizontpt = pd.read_csv(r'/media/pablo/LocalData/visibilityAnalysis/horizon_points.csv')
hor_coord = horizontpt[['X','Y','Z1']]
horcoord_0 = horizontpt.fillna(0)


#getting the coords of the antenna

antpt = pd.read_csv(r'/media/pablo/LocalData/visibilityAnalysis/horizon_antena.csv')
ant_coord = antpt[['AX','AY','AZ']]


az1 = azimuthandaltitud(hor_coord,ant_coord)

ax1 = az1.plot.scatter(x='az',
...                    y='Z1',
...                    c='DarkBlue')
ax2 = az1.plot.scatter(x='az',
...                    y='Z1',
...                    c='DarkBlue')